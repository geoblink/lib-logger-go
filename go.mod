module bitbucket.org/geoblink/lib-logger-go

go 1.12

require (
	github.com/pkg/errors v0.8.1
	github.com/rs/zerolog v1.19.0
	github.com/stretchr/testify v1.6.1
)
