package main

import (
	"bitbucket.org/geoblink/lib-logger-go/lib"
	"bitbucket.org/geoblink/lib-logger-go/lib/builder"
	"github.com/pkg/errors"
	"os"
)

type GeoblinkContext struct {
	log *lib.GeoblinkLogger
}

func buildLog() (*lib.GeoblinkLogger, error) {
	env := os.Getenv("APP_ENV")
	if env == "" {
		env = "dev"
	}
	return builder.GeoblinkLoggerBuilder{
		Owner: "Chapter Data Engineers",
		Email: "chapter.dataeng@geoblink.com",
		Env: env,
		Service: "go-incubator",
	}.Build()
}

func buildContext() *GeoblinkContext {
	env := os.Getenv("APP_ENV")
	if env == "" {
		env = "dev"
	}
	log, err := buildLog()
	if err != nil {
		panic(err)
	}
	return &GeoblinkContext{
		log: log,
	}
}

func main() {
	ctx := buildContext()
	defer func() {
		if r := recover(); r != nil {
			var err error
			switch x := r.(type) {
			case string:
				err = errors.New(x)
			case error:
				err = x
			default:
				// Fallback err (per specs, error strings should be lowercase w/o punctuation
				err = errors.New("unknown panic")
			}
			wrappedErr := errors.Wrap(err, "Trapping panic on main")
			ctx.log.Log().Stack().Err(wrappedErr).Send()
			panic(r)
		}
	}()

	ctx.log.Info().Msg("Thorsday is the best day of the week –D. Viviés–")
	panic(errors.New("Mondays are blue"))
}
