package builder

import (
	"bitbucket.org/geoblink/lib-logger-go/lib"
	"github.com/rs/zerolog"
	"github.com/stretchr/testify/assert"
	"testing"
)

// This is an stupid test only for assuring the types are right
func TestGeoblinkLoggerBuilder_Build(t *testing.T) {

	t.Run("testing interface", func(t *testing.T) {
		builder, err := GeoblinkLoggerBuilder{
			Owner:   "Test",
			Email:   "test@test.io",
			Env:     "test",
			Service: "go-incubator",
		}.Build()

		assert.NoError(t, err, "No error should be returned by the builder")
		assert.IsType(t, lib.GeoblinkLogger{}, *builder, "Built logger should be a GeoblinkLogger")
	})

	t.Run("testing level set properly", func(t *testing.T) {
		builder, err := GeoblinkLoggerBuilder{
			Owner:   "Test",
			Email:   "test@test.io",
			Env:     "test",
			Level:   "debug",
			Service: "go-incubator",
		}.Build()

		assert.NoError(t, err, "No error should be returned by the builder")
		assert.IsType(t, lib.GeoblinkLogger{}, *builder, "Built logger should be a GeoblinkLogger")
		assert.Equal(t, zerolog.DebugLevel, builder.GetLevel())
	})

	t.Run("testing level set to info as default", func(t *testing.T) {
		builder, err := GeoblinkLoggerBuilder{
			Owner:   "Test",
			Email:   "test@test.io",
			Env:     "test",
			Service: "go-incubator",
		}.Build()

		assert.NoError(t, err, "No error should be returned by the builder")
		assert.IsType(t, lib.GeoblinkLogger{}, *builder, "Built logger should be a GeoblinkLogger")
		assert.Equal(t, zerolog.InfoLevel, builder.GetLevel())
	})

	t.Run("testing wrong level set to info as fallback", func(t *testing.T) {
		builder, err := GeoblinkLoggerBuilder{
			Owner:   "Test",
			Email:   "test@test.io",
			Env:     "test",
			Level:   "test",
			Service: "go-incubator",
		}.Build()

		assert.NoError(t, err, "No error should be returned by the builder")
		assert.IsType(t, lib.GeoblinkLogger{}, *builder, "Built logger should be a GeoblinkLogger")
		assert.Equal(t, zerolog.InfoLevel, builder.GetLevel())
	})

}
