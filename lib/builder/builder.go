package builder

import (
	"bitbucket.org/geoblink/lib-logger-go/lib"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/pkgerrors"
	"os"
)

// Exposes the structure of the builder in charge of creating a new logger instance.
// Each value is directly connected to a field on the result logger message.
type GeoblinkLoggerBuilder struct {
	Owner      string `json:"owner"`
	Email      string `json:"email"`
	Env        string `json:"env"`
	Level      string `json:"level"`
	Repository string `json:"repository"`
	Service    string `json:"service"`
}

// Builds –and initialize– a logger instance ready to be consumed by the application.
// It uses the facade `GeoblinkLogger`. Meaning? If the facade determines that the underlying library
// must be changed, the usage may –most likely will– change.
// It returns a valid `GeoblinkLoggger` if everything worked fine, error otherwise.
func (gl GeoblinkLoggerBuilder) Build() (*lib.GeoblinkLogger, error) {
	zerolog.ErrorStackMarshaler = pkgerrors.MarshalStack
	if gl.Level == "" {
		gl.Level = "info"
	}
	parsedLvl, err := zerolog.ParseLevel(gl.Level); if err != nil {
		parsedLvl = zerolog.InfoLevel
	}
	logger := zerolog.New(os.Stdout).
		Level(parsedLvl).
		With().Timestamp().Logger().
		With().Caller().Logger().
		With().Str("owner", gl.Owner).Logger().
		With().Str("email", gl.Email).Logger().
		With().Str("env", gl.Env).Logger().
		With().Str("repository", gl.Repository).Logger().
		With().Str("service", gl.Service).Logger().
		With().Str("source", "go").Logger()

	return &logger, nil
}
