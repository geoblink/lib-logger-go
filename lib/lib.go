package lib

import "github.com/rs/zerolog"

// `GeoblinkLogger` is an alias to `zerolog.Logger`.
// It acts as a facade for the actual logger class, `zerlog.Logger`.
type GeoblinkLogger = zerolog.Logger

