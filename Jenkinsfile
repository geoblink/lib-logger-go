pipeline {

  options {
    disableConcurrentBuilds()
    buildDiscarder(logRotator(daysToKeepStr: '7'))
    timestamps()
    ansiColor('xterm')
  }

  agent { kubernetes { yamlFile 'KubernetesPod.yaml' } }

  environment {
    BITBUCKET_HTTP_CREDS = credentials('bitbucket-http-userpass')
  }

  stages {
    stage('Test Library') {
      when { anyOf { branch 'master'; branch 'PR-*' } }
      steps {
        container('go') {
          sh "apk add --no-cache git gcc musl-dev"

          sh "go get -v"
          sh "go build -v ./..."
          sh """
            go get -u github.com/jstemmer/go-junit-report
            # this library helps exporting junit xml reports
          """
          sh "go test -v ./... | go-junit-report > test-results.xml"
        }
      }
    }

    stage('Publish Library') {
      when { branch 'master' }
      steps {
        container('base') {
          sh "JG_DEBUG=1 jg create-tag"
        }
      }
    }
  }

  post {
    always {
      archiveArtifacts artifacts: 'test-results.xml', onlyIfSuccessful: false, allowEmptyArchive: true
      junit 'test-results.xml'
      cleanWs()
    }
  }
}