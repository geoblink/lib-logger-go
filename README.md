# lib-logger-go
This library exposes a wrapper over `github.com/rs/zerolog`'s `Logger`, 
giving a common logging interface to all Geoblink's programs written in `go`.

## Motivation
In order to standarize the log format of our applications, Geoblink has defined a common interface for our logs.
This library tries to adapt that new format.

> :warning: For the very first iteration, we are not following 100% the same format. Instead of having filename and line in two different fields, we have only one called `caller`. It might change in the future.
## Installation
### go get
`go get bitbucket.org/geoblink/lib-logger-go`

### go modules
Insert the following on your `go.mod` file:
```go
module <your-module>

go <go-version>

require (
    [...]
	bitbucket.org/geoblink/lib-logger-go v0.0.0
    [...]
)

```

## Usage
> Note: JSONs are expanded for demonstration purposes. 

> We've created a [`main.go`](main.go) file with an example of use.

First of all, build a `GeoblinkLogger` via:
```go
logger, err := GeoblinkLoggerBuilder{
    Owner: "<owner>",
    Email: "<email>",
    Env: "<env>",
    Service: "<service>",
}.Build()

if err != nil {
    panic(err)
}
```
For easy messaging, such as _debug_, _info_, or similar, execute 
```go
logger.Info().Msg("Thorsday is the best day of the week –D. Viviés–")
```
It produces
```json
{
    "level": "info",
    "owner": "<owner>",
    "email": "<email>",
    "env": "<env>",
    "host": "<host>",
    "repository": "<repository>",
    "service": "<service>",
    "source": "go",
    "time": "2020-06-05T16:00:05+02:00",
    "caller": "[...]/lib-logger-go/main.go:61",
    "message": "Thorsday is the best day of the week –D. Viviés–"
}
```

For handling errors with the _stacktrace_, a bit of work must be done. Assuming that you have the error happened:
> Using library `github.com/pkg/errors` for `errors.Wrap`

```go
wrappedErr := errors.Wrap(err, "Trapping panic on main")
logger.Log().Stack().Err(wrappedErr).Send() 
```

It produces:
```json
{
    "owner": "<owner>",
    "email": "<email>",
    "env": "<env>",
    "host": "<host>",
    "repository": "<repository>",
    "service": "<service>",
    "source": "go",
    "stack": [
        {
            "func": "main.func1",
            "line": "55",
            "source": "main.go"
        },
        {
            "func": "gopanic",
            "line": "522",
            "source": "panic.go"
        },
        {
            "func": "main",
            "line": "62",
            "source": "main.go"
        },
        [...]
    ],
    "error": "Trapping panic on main: Mondays are blue",
    "time": "2020-06-05T16:00:05+02:00",
    "caller": "[...]/lib-logger-go/main.go:56"
}
```